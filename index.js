// console.log("Hello!");

/*
	JSON Objects
		- JSON stands for JavaScript Object Notation
		- JSON is also used in other programming languages
		- JavaScript Objects are not to be confused with JSON

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON Objects 
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// // JSON Array
// "cities": [
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	}
// ]

// JSON Methods
	// The JSON Object contains methods for parsing and converting data into stringified JSON
	// So files to be sent are lightweight

// Converting Data into a stringified JSON
let batchesArr = [
	{ batchname: "Batch 204"},
	{ batchname: "Batch 203"}
];

console.log(batchesArr);
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "John",
	age: 18,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data);

// User Details
// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country does your city belong to?")
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// });

// console.log(otherData);

// Convert stringified JSON into JS Objects
	// JSON.parse()
let batchesJSON = '[{"batchName": "Batch204"}, {"batchName": "Batch203"}]';

console.log(batchesJSON);
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age": "18",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`

console.log(JSON.parse(stringifiedObject));